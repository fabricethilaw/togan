package com.qanbio.mobile.evote;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;



/**
 * Created by Thilaw Fabrice on 2016-06-07.
 */
public class LightTextView extends TextView {

    public LightTextView(Context context) {
        super(context);
        init();
    }

    public LightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LightTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super( context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
     //   if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Assets.FONT_ASSET_ROBOTO_LIGHT);
            setTypeface(tf, 0);
      //  }

    }
}
